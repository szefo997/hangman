import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {Router} from "@angular/router";
import {APP_ROUTES} from "./app.routing";
import {NbDialogModule, NbLayoutModule, NbThemeModule} from "@nebular/theme";
import {NbEvaIconsModule} from "@nebular/eva-icons";
import {BrowserModule} from "@angular/platform-browser";
import {RouterTestingModule} from "@angular/router/testing";
import {NgxsModule} from "@ngxs/store";
import {NgZone} from "@angular/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('AppComponent', () => {
  let router: Router;
  let ngZone: NgZone;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
      ],
      imports: [
        BrowserModule,
        BrowserAnimationsModule,

        RouterTestingModule.withRoutes(APP_ROUTES),
        NgxsModule.forRoot([]),
        NbThemeModule.forRoot({name: 'default'}),
        NbDialogModule.forRoot(),
        NbLayoutModule,
        NbEvaIconsModule
      ]
    }).compileComponents();

    router = TestBed.get(Router);
    ngZone = TestBed.get(NgZone);
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should be able to navigate to `/`', async () => {
    await ngZone.run(() =>
      router.navigate(['/'])
        .then(() => {
          expect(router.url).toEqual('/');
        }));
  });

  it('should be able to navigate to `/game`', async () => {
    await ngZone.run(() =>
      router.navigate(['/game'])
        .then(() => {
          expect(router.url).toEqual('/game');
        }));
  });

});
