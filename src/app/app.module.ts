import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {APP_ROUTES} from "./app.routing";
import {NgxsModule} from "@ngxs/store";
import {NbDialogModule, NbLayoutModule, NbThemeModule} from "@nebular/theme";
import {NbEvaIconsModule} from "@nebular/eva-icons";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    RouterModule.forRoot(
      APP_ROUTES, {enableTracing: false}
    ),

    NgxsModule.forRoot([], {developmentMode: true}),

    NbThemeModule.forRoot({name: 'dark'}),
    NbDialogModule.forRoot(),
    NbEvaIconsModule,
    NbLayoutModule
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
