import {Action, Selector, State, StateContext} from '@ngxs/store';
import {Observable} from 'rxjs';
import {Char, Letter} from "./models/letter";
import {GameService} from "./services/game.service";
import {debounceTime, finalize, tap} from "rxjs/operators";
import {patch, updateItem} from "@ngxs/store/operators";

export class GameFetchWords {
  static readonly type = '[Game] fetch words';

  constructor() {
  }
}

export class GameCheckLetter {
  static readonly type = '[Game] check letter';

  constructor(public letter: Char) {
  }
}

export class GamePlayAgain {
  static readonly type = '[Game] play again';

  constructor() {
  }
}

export interface GameStateModel {
  letters: Letter[];
  points: number;
  loading: boolean;
  words: string[];
  randomWords: Letter[][];
  randomWord: Letter[];
  win: boolean;
  level: number;
  loadedLevel: boolean;
  requiredLevelToWin: number;
  time: number;
}

export const defaultsGameStateModel = {
  letters: GameService.getLetters(),
  points: 0,
  loading: false,
  words: [],
  randomWords: null,
  randomWord: null,
  win: false,
  level: 1,
  requiredLevelToWin: 5,
  loadedLevel: true,
  time: new Date().getTime()
};

@State<GameStateModel>({
  name: 'game',
  defaults: defaultsGameStateModel
})
export class GameState {

  constructor(
    private _gameService: GameService
  ) {
  }

  @Selector()
  static level({level}: GameStateModel): number {
    return level;
  }

  @Selector()
  static time({time}: GameStateModel): number {
    return time;
  }

  @Selector()
  static requiredLevelToWin({requiredLevelToWin}: GameStateModel): number {
    return requiredLevelToWin;
  }

  @Selector()
  static win({win}: GameStateModel): boolean {
    return win;
  }

  @Selector()
  static loading({loading}: GameStateModel): boolean {
    return loading;
  }

  @Selector()
  static points({points}: GameStateModel): number {
    return points;
  }

  @Selector()
  static randomWord({randomWord}: GameStateModel): Letter[] {
    return randomWord;
  }

  @Selector()
  static letters({letters}: GameStateModel): Letter[] {
    return letters;
  }

  @Selector()
  static loadedLevel({loadedLevel}: GameStateModel): boolean {
    return loadedLevel;
  }

  @Action(GameFetchWords)
  fetchWords({patchState}: StateContext<GameStateModel>): Observable<string[]> {
    patchState({loading: true});
    return this._gameService.fetchWords()
      .pipe(
        tap(words => {
          patchState({
            words: Object.assign([], words),
          });
          const randomWords = this._getRandomWords(words);
          console.log('randomWords[0]', randomWords[0]);
          patchState({
              randomWords,
              randomWord: randomWords[0]
            }
          );
        }),
        debounceTime(1000),
        finalize(() => {
          patchState({loading: false});
        })
      );
  }

  @Action(GameCheckLetter)
  checkLetter({patchState, getState, setState}: StateContext<GameStateModel>, {letter}: GameCheckLetter): Promise<any> {
    this._checkLettersOnRandomWord(patchState, getState, letter);
    this._checkLetterOnAvailableLetters(setState, letter);
    this._setPoints(getState, letter, patchState);
    return this._goToNextLevelIfCompleteWord(getState, patchState);
  }

  @Action(GamePlayAgain)
  async playAgain({patchState, getState}: StateContext<GameStateModel>): Promise<any> {
    const {words, letters} = getState();
    const randomWords = this._getRandomWords(Object.assign([], words));

    patchState({loadedLevel: false});
    patchState({
      letters: this._clearLetters(letters),
      randomWord: randomWords[0],
      randomWords,
      points: 0,
      time: new Date().getTime(),
      level: 1
    });

    await this._waitForAnimation();
    patchState({loadedLevel: true});
  }

  private async _waitForAnimation(): Promise<any> {
    await new Promise(resolve => {
      setTimeout(() => {
        resolve();
      }, 450)
    });
  }

  private _getRandomWords(words): Letter[][] {
    const arr: Letter[][] = [];
    for (let i = 0; i < 5; i++) {
      const rand = Math.floor(Math.random() * words.length);
      arr.push(words[rand].split('')
        .map(v => ({
          char: v.toUpperCase() as Char,
          checked: false
        })));
      arr.splice(rand, 1);
    }
    return arr;
  }

  private async _goToNextLevelIfCompleteWord(getState: () => GameStateModel, patchState: (val: Partial<GameStateModel>) => GameStateModel) {
    const {randomWord, level, randomWords, letters, requiredLevelToWin, time} = getState();
    const filtered = randomWord.filter(v => v.checked);

    if (filtered.length === randomWord.length && level === requiredLevelToWin) {
      patchState({
        win: true,
        time: new Date().getTime() - time
      });
    } else if (filtered.length === randomWord.length) {
      patchState({loadedLevel: false});
      patchState({
        letters: this._clearLetters(letters),
        randomWord: randomWords[level],
        level: level + 1
      });

      await this._waitForAnimation();
      patchState({loadedLevel: true});
    }
  }

  private _clearLetters(letters: Letter[]): Letter[] {
    return letters.map(value => {
      const obj = Object.assign({}, value);
      obj.checked = false;
      return obj;
    });
  }

  private _checkLetterOnAvailableLetters(setState, letter): void {
    setState(
      patch({
        letters: updateItem<Letter>(l => l.char === letter, patch<Letter>({checked: true})),
      })
    );
  }

  private _checkLettersOnRandomWord(patchState: (val: Partial<GameStateModel>) => GameStateModel, getState: () => GameStateModel, char: Char): void {
    const {randomWord} = getState();
    patchState({
      randomWord: randomWord.map(l => {
        const obj = Object.assign({}, l);
        if (!obj.checked) {
          obj.checked = obj.char === char;
        }
        return obj;
      })
    });
  }

  private _setPoints(getState: () => GameStateModel, letter, patchState: (val: Partial<GameStateModel>) => GameStateModel): void {
    const {points, randomWord} = getState();
    // id doesn't found then increase points
    if (!randomWord.find(value => value.char === letter)) {
      patchState({points: (points + 1)})
    }
  }

}





