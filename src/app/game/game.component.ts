import {animate, style, transition, trigger} from '@angular/animations';
import {AfterViewInit, Component, OnDestroy, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {NbDialogService} from '@nebular/theme';
import {Select, Store} from '@ngxs/store';
import {Observable, Subscription} from "rxjs";
import {GameCheckLetter, GameFetchWords, GamePlayAgain, GameState} from "./game.state";
import {Letter} from "./models/letter";

@Component({
  selector: 'rb-game',
  template: `
    <ng-template #dialog let-data let-ref="dialogRef">
      <nb-card>
        <nb-card-header>{{data.title}}</nb-card-header>
        <nb-card-body>
          <span [innerHtml]="data.message"></span>
        </nb-card-body>
        <nb-card-footer>
          <button class="m-2" nbButton (click)="ref.close(true)">Yes</button>
          <button class="m-2" nbButton (click)="ref.close(false)">No</button>
        </nb-card-footer>
      </nb-card>
    </ng-template>
    <nb-card *ngIf="loadedLevel$ | async" [@enterAnimation] [nbSpinner]="loading$ | async" nbSpinnerStatus="danger">

      <nb-card-body>
        <div class="row">
          <div class="col-12">
                      <span>Level: <span
                        class="js-level">{{level$ |async}}</span>/<span
                        class="js-required">{{requiredLevelToWin$| async}}</span></span>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <h5 class="js-letters text-center">Available letters</h5>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <ul class="js-available text-center">
              <li *ngFor="let l of letters$ | async">
                <button [disabled]="l.checked" nbButton size="medium" shape="semi-round"
                        status="info"
                        appearance="filled"
                        (click)="onCheck(l)">
                  {{l.char}}
                </button>
              </li>
            </ul>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <ul class="text-center">
              <li *ngFor="let letter of randomWord$ | async" class="underlined-letter">
                <span class="letter" *ngIf="letter.checked">{{letter.char}}</span>
                <span class="underline"></span>
              </li>
            </ul>
          </div>
        </div>

        <div class="row">
          <div class="col-12">

            <!-- Hangman -->
            <svg width="500" height="550">
              <!-- Hanger -->
              <polyline points="400,500 100,500 150,500 150,100 320,100 320,150"/>

              <!-- body -->
              <line *ngIf="(points$  | async) >= 2" x1="320" y1="190" x2="320" y2="360"/>
              <!-- left arm -->
              <line *ngIf="(points$  | async) >= 3" x1="320" y1="260" x2="250" y2="230"/>
              <!-- Right arm -->
              <line *ngIf="(points$  | async) >= 4" x1="320" y1="260" x2="390" y2="230"/>
              <!-- left leg -->
              <line *ngIf="(points$  | async) >= 5" x1="320" y1="360" x2="250" y2="450"/>
              <!-- Right leg -->
              <line *ngIf="(points$  | async) >= 6" x1="320" y1="360" x2="390" y2="450"/>
              <!-- head -->
              <circle *ngIf="(points$  | async) >= 1" cx="320" cy="190" r="40" fill="white"/>
            </svg>
          </div>
        </div>

      </nb-card-body>
    </nb-card>
  `,
  styleUrls: ['./game.component.scss'],
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({transform: 'translateX(-100%)', opacity: 0}),
          animate('450ms', style({transform: 'translateX(0)', opacity: 1}))
        ]),
        transition(':leave', [
          style({transform: 'translateX(0)', opacity: 1}),
          animate('450ms', style({transform: 'translateX(100%)', opacity: 0}))
        ])
      ]
    )
  ]
})
export class GameComponent implements OnInit, AfterViewInit, OnDestroy {


  @Select(GameState.level) public level$: Observable<number>;
  @Select(GameState.requiredLevelToWin) public requiredLevelToWin$: Observable<number>;
  @Select(GameState.loadedLevel) public loadedLevel$: Observable<boolean>;
  @Select(GameState.win) public win$: Observable<boolean>;
  @Select(GameState.letters) public letters$: Observable<Letter[]>;
  @Select(GameState.loading) public loading$: Observable<boolean>;
  @Select(GameState.points) public points$: Observable<number>;
  @Select(GameState.randomWord) public randomWord$: Observable<Letter[]>;

  @ViewChild('dialog', {static: false}) dialog: TemplateRef<any>;

  private _subs: Subscription;

  constructor(private _nbDialogService: NbDialogService,
              private _store: Store) {
  }

  ngOnInit() {
    this._store.dispatch(new GameFetchWords());
  }

  ngAfterViewInit(): void {
    const pointsSubs = this.points$.subscribe(value => {
      if (value === 6) {
        this.open(false);
      }
    });

    const winSubs = this.win$.subscribe(value => {
      if (value) {
        this.open(true);
      }
    });

    this._subs.add(pointsSubs);
    this._subs.add(winSubs);
  }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public onCheck({char}: Letter): void {
    this._store.dispatch(new GameCheckLetter(char));
  }

  public open(win: boolean) {
    const diff = this._store.selectSnapshot(GameState.time);
    const minutes = Math.round(diff / 60000);
    const seconds = Math.round((diff % 60000) / 600);
    const context = win ? {
      title: 'You won !!!',
      message: `Congratulations. <br>
             Your time <br>
             ${minutes} minutes. <br>
             ${seconds} seconds. <br>
             Do you want play again?`
    } : {
      title: 'Sorry You lose',
      message: 'Do you want play again?'
    };

    const closeSub: Subscription = this._nbDialogService.open(this.dialog, {
      closeOnBackdropClick: false,
      closeOnEsc: true,
      context: context
    }).onClose.subscribe((playAgain: boolean) => {
      if (playAgain) {
        this._store.dispatch(new GamePlayAgain());
      }
      closeSub.unsubscribe();
    });
  }

}
