import {GameComponent} from "./game.component";
import {Routes} from "@angular/router";

export const GAME_ROUTES: Routes = [{
  path: '',
  component: GameComponent
}];
