import {async, TestBed} from '@angular/core/testing';
import {NgxsModule, Store} from "@ngxs/store";
import {GameState, GameStateModel} from "./game.state";
import {GameService} from "./services/game.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {Letter} from "./models/letter";

const DEFAULT_STATE: GameStateModel = {
  letters: GameService.getLetters(),
  points: 0,
  loading: false,
  words: [
    "Afghanistan",
    "Albania",
    "Algeria",
    "Andorra",
    "Angola",
    "Antigua",
    "Argentina",
    "Armenia",
    "Aruba",
    "Australia",
    "Austria",
    "Azerbaijan",
    "Bahrain",
    "Bangladesh",
    "Barbados",
    "Belarus",
    "Belgium",
    "Belize",
    "Benin",
    "Bhutan",
    "Bolivia",
    "Bosnia",
    "Botswana",
    "Brazil",
    "Brunei",
    "Bulgaria",
    "Burkina",
    "Burma",
    "Burundi",
    "Cambodia"],
  randomWords: [[
    {char: "M", checked: false},
    {char: "O", checked: false},
    {char: "R", checked: false},
    {char: "O", checked: false},
    {char: "C", checked: false},
    {char: "C", checked: false},
    {char: "O", checked: false}
  ], [
    {char: "M", checked: false},
    {char: "A", checked: false},
    {char: "D", checked: false},
    {char: "A", checked: false},
    {char: "G", checked: false},
    {char: "A", checked: false},
    {char: "S", checked: false},
    {char: "C", checked: false},
    {char: "A", checked: false},
    {char: "R", checked: false}
  ], [
    {char: "P", checked: false},
    {char: "O", checked: false},
    {char: "L", checked: false},
    {char: "A", checked: false},
    {char: "N", checked: false},
    {char: "D", checked: false}
  ]],
  randomWord: [
    {char: "M", checked: false},
    {char: "O", checked: false},
    {char: "R", checked: false},
    {char: "O", checked: false},
    {char: "C", checked: false},
    {char: "C", checked: false},
    {char: "O", checked: false}
  ],
  win: false,
  level: 1,
  requiredLevelToWin: 3,
  loadedLevel: true,
  time: new Date().getTime()
};


describe('GameState', () => {
  let store: Store;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        NgxsModule.forRoot([GameState], {
          developmentMode: true
        })
      ],
      providers: [GameService]
    }).compileComponents();

    store = TestBed.get(Store);
    store.reset(DEFAULT_STATE);
  }));

  it('win should be false', (() => {
    const win: boolean = store.selectSnapshot((state: GameStateModel) => state.win);
    expect(win).toBe(DEFAULT_STATE.win);
  }));

  it('points should be equal to default', (() => {
    const points: number = store.selectSnapshot((state: GameStateModel) => state.points);
    expect(points).toBe(DEFAULT_STATE.points);
  }));

  it('words should exist', (() => {
    const words: string[] = store.selectSnapshot((state: GameStateModel) => state.words);
    expect(words).not.toBeNull();
    expect(words.length).toBe(DEFAULT_STATE.words.length);
  }));

  it('requiredLevelToWin should be equal to default', (() => {
    const requiredLevelToWin: number = store.selectSnapshot((state: GameStateModel) => state.requiredLevelToWin);
    expect(requiredLevelToWin).toBe(DEFAULT_STATE.requiredLevelToWin);
  }));

  it('loadedLevel should be true', (() => {
    const loadedLevel: boolean = store.selectSnapshot((state: GameStateModel) => state.loadedLevel);
    expect(loadedLevel).toBe(DEFAULT_STATE.loadedLevel);
  }));

  it('random words should exist', async(async () => {
    const randomWord: Letter[] = store.selectSnapshot((state: GameStateModel) => state.randomWord);
    expect(randomWord).not.toBeNull();
    expect(randomWord.length).toBe(DEFAULT_STATE.randomWord.length);
  }));

  it('time should be equal to default time', async(async () => {
    const time: number = store.selectSnapshot((state: GameStateModel) => state.time);
    expect(time).not.toBeNull();
    expect(time).toBe(DEFAULT_STATE.time);
  }));

  // doesn't reset correctly state
  // it('it should check letter from available letter', async(async () => {
  // const char = 'A';
  // store.reset(DEFAULT_STATE);
  // await store.dispatch(new GameCheckLetter(char)).toPromise();
  // const letters: Letter[] = store.selectSnapshot((state: GameStateModel) => state.letters);
  // const randomWord: Letter[] = store.selectSnapshot((state: GameStateModel) => state.randomWord);
  // console.log('randomWord', randomWord);
  // console.log('letters', letters);
  // const find: Letter = letters.find(l => l.char === char);
  // expect(find).not.toBeNull();
  // expect(find.char).toBe(char);
  // expect(find.checked).toBe(true);
  // }));

  // it('it should check letter from random word', async(async () => {
  //   const char = 'M';
  //   // await store.dispatch(new GameSetState(DEFAULT_STATE)).toPromise();
  //   await store.dispatch(new GameCheckLetter(char)).toPromise();
  //   const letters: Letter[] = store.selectSnapshot((state: GameStateModel) => state.randomWord);
  //   console.log('letters', letters);
  //   const find: Letter[] = letters.filter(l => l.char === char);
  //   expect(find).not.toBeNull();
  //   expect(find.length).toBe(1);
  //   expect(find[0].checked).toBe(true);
  //   expect(find[0].char).toBe(char);
  // }));

});
