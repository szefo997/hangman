import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GameComponent} from './game.component';
import {NgxsModule} from "@ngxs/store";
import {RouterModule} from "@angular/router";
import {GAME_ROUTES} from "./game.routing";
import {NbButtonModule, NbCardModule, NbDialogModule, NbSpinnerModule} from "@nebular/theme";
import {GameState} from "./game.state";
import {GameService} from "./services/game.service";
import {HttpClientModule} from "@angular/common/http";


@NgModule({
  declarations: [GameComponent],
  imports: [
    CommonModule,
    HttpClientModule,

    NgxsModule.forFeature([GameState]),
    RouterModule.forChild(GAME_ROUTES),
    NbDialogModule.forChild(),
    NbCardModule,
    NbButtonModule,
    NbSpinnerModule
  ],
  providers: [GameService]
})
export class GameModule {
}
