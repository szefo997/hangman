import {getTestBed, TestBed} from '@angular/core/testing';

import {GameService} from './game.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('GameService', () => {
  let injector: TestBed;
  let gameService: GameService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed
      .configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [GameService]
      });

    injector = getTestBed();
    gameService = injector.get(GameService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(gameService).toBeTruthy();
  });

  it('letters should be defined', () => {
    const letters = GameService.getLetters();
    expect(letters).toBeDefined();
    expect(letters.length).toBeGreaterThan(0);
  });

  it('should return an Observable<string[]>', () => {
    const dummyWordList = ['John', 'Doe'];

    gameService.fetchWords().subscribe(wordList => {
      expect(wordList.length).toBe(2);
      expect(wordList).toEqual(dummyWordList);
    });

    const req = httpMock.expectOne(`./assets/answers.json`);
    expect(req.cancelled).toBeFalsy();
    expect(req.request.responseType).toEqual('json');
    expect(req.request.method).toBe("GET");
    req.flush(dummyWordList);
  });


});
