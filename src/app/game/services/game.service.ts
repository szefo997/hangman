import {Injectable} from '@angular/core';
import {Char, Letter} from "../models/letter";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

const letters: Char[] = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

@Injectable()
export class GameService {

  constructor(private _httpClient: HttpClient) {
  }

  public static getLetters = (): Letter[] => letters.map(char => ({char, checked: false}));

  public fetchWords(): Observable<string[]> {
    return this._httpClient.get<string[]>(`./assets/answers.json`);
  }

}
