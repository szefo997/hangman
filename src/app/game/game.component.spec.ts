import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GameComponent} from './game.component';
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {GAME_ROUTES} from "./game.routing";
import {NbButtonModule, NbCardModule, NbDialogModule, NbSpinnerModule, NbThemeModule} from "@nebular/theme";
import {GameService} from "./services/game.service";
import {NgxsModule} from "@ngxs/store";
import {GameState} from "./game.state";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {By} from "@angular/platform-browser";
import {DebugElement} from "@angular/core";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

describe('GameComponent', () => {
  let component: GameComponent;
  let fixture: ComponentFixture<GameComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed
      .configureTestingModule({
        declarations: [GameComponent],
        imports: [
          CommonModule,
          HttpClientTestingModule,
          BrowserAnimationsModule,

          NgxsModule.forRoot([GameState], {
            developmentMode: true
          }),
          RouterModule.forRoot(GAME_ROUTES),

          NbThemeModule.forRoot({name: 'corporate'}),
          NbDialogModule.forRoot(),
          NbCardModule,
          NbButtonModule,
          NbSpinnerModule
        ],
        providers: [GameService]
      })
      .compileComponents();

    fixture = TestBed.createComponent(GameComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have letters', () => {
    component.letters$.subscribe(value => {
      expect(value).toBeDefined();
      expect(value.length).toBeGreaterThan(0);
    });
  });

  it('loading should be defined', () => {
    component.loading$.subscribe(value => {
      expect(value).toBeDefined();
    });
  });

  it('points should be zero', () => {
    component.points$.subscribe(value => {
      expect(value).toBe(0);
    });
  });

  it('win should be false', () => {
    component.win$.subscribe(value => {
      expect(value).toBe(false);
    });
  });

  it('level should be 1', () => {
    component.level$.subscribe(value => {
      expect(value).toBe(1);
      const title = debugElement.query(By.css('.js-level')).nativeElement.innerText;
      expect(title).toBeDefined();
      expect(title).toEqual('1');
    });
  });

  it('random word should be defined', () => {
    component.randomWord$.subscribe(value => {
      expect(value).toBeDefined();
      expect(value.length).toBeGreaterThan(0);
    });
  });

  it('required level to win should be 5', () => {
    component.requiredLevelToWin$.subscribe(value => {
      expect(value).toBeDefined();
      expect(value).toBe(5);
    });
  });

  it('loaded level should be true', () => {
    component.loadedLevel$.subscribe(value => {
      expect(value).toBeDefined();
      expect(value).toBe(true);
    });
  });

  it('it should be correct message above letters', () => {
    const title = debugElement.query(By.css('.js-letters')).nativeElement.innerText;
    expect(title).toBeDefined();
    expect(title).toEqual('Available letters');
  });

});
