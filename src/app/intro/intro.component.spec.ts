import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {IntroComponent} from './intro.component';
import {Router} from "@angular/router";
import {INTRO_ROUTES} from "./intro.routing";
import {NbButtonModule, NbCardModule, NbThemeModule} from "@nebular/theme";
import {CommonModule} from "@angular/common";
import {DebugElement} from "@angular/core";
import {By} from "@angular/platform-browser";
import {RouterTestingModule} from "@angular/router/testing";

describe('IntroComponent', () => {
  let component: IntroComponent;
  let fixture: ComponentFixture<IntroComponent>;
  let debugElement: DebugElement;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IntroComponent],
      imports: [
        CommonModule,

        RouterTestingModule.withRoutes(INTRO_ROUTES),
        NbThemeModule.forRoot({name: 'default'}),
        NbCardModule,
        NbButtonModule
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(IntroComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  }));

  it('it should be correct intro message', () => {
    const title = debugElement.query(By.css('.js-title')).nativeElement.innerText;
    expect(title).toBeTruthy();
    expect(title).toEqual('Welcome to hangman!');
  });

  it('it should correct button name', () => {
    const button = debugElement.query(By.css('.js-play')).nativeElement.innerText;
    expect(button).toBeTruthy();
    expect(button.toUpperCase()).toEqual('PLAY');
  });

  it('it should navigate to game route', () => {
    const navigateSpy = spyOn(router, 'navigate');
    component.onPlay();
    expect(navigateSpy).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalledWith(['/game']);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
