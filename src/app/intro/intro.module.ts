import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IntroComponent} from './intro.component';
import {RouterModule} from "@angular/router";
import {INTRO_ROUTES} from "./intro.routing";
import {NgxsModule} from "@ngxs/store";
import {NbButtonModule, NbCardModule} from "@nebular/theme";

@NgModule({
  declarations: [IntroComponent],
  imports: [
    CommonModule,

    NgxsModule.forFeature([]),
    RouterModule.forChild(INTRO_ROUTES),

    NbCardModule,
    NbButtonModule
  ]
})
export class IntroModule {
}
