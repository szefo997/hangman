import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'rb-intro',
  template: `
      <nb-card>
          <nb-card-body>
              <h2 class="js-title text-center">Welcome to hangman!</h2>
              <br>
              <div class="row">
                  <div class="col-12 text-center">
                      <button class="js-play" nbButton size="giant" shape="semi-round" (click)="onPlay()">
                          Play
                      </button>
                  </div>
              </div>
          </nb-card-body>
      </nb-card>
  `,
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {

  constructor(private _router: Router) {
  }

  ngOnInit() {
  }

  public onPlay(): void {
    this._router.navigate(['/game']);
  }

}
