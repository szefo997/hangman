import {Component} from '@angular/core';

@Component({
  selector: 'rb-root',
  template: `
      <nb-layout center>
          <nb-layout-column>
              <router-outlet></router-outlet>
          </nb-layout-column>
      </nb-layout>
  `
})
export class AppComponent {
}
